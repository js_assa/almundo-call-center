package com.almundo.app.model;

import com.almundo.app.util.Rol;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

/**
 * Created by sebastian on 5/8/19.
 */
@Document(collection = "employee")
@JsonIgnoreProperties(value = { "busy" })
public class Employee implements Model  {
    @Id
    @NotNull
    private ObjectId id;
    private String name;
    private Rol rol;
    private boolean busy;

    public Employee() {
    }

    public Employee(String name, Rol rol) {
        this.name = name;
        this.rol = rol;
        this.busy = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public boolean isBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        this.busy = busy;
    }
}
