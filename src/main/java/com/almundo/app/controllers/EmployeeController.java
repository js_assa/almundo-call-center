package com.almundo.app.controllers;

import com.almundo.app.model.Employee;
import com.almundo.app.model.Response;
import com.almundo.app.repository.EmployeeRepository;
import com.almundo.app.services.EmployeeService;
import com.almundo.app.util.Rol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by sebastian on 5/8/19.
 */
@RestController
@RequestMapping("employees")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping(value = "/")
    ResponseEntity<List<Employee>> get(@RequestParam(required = false) String name) {
        List<Employee> employees = new ArrayList<>();
        if (name != null) {
            employees = this.employeeRepository.findByName(name);
        } else {
            employees = this.employeeRepository.findAll();
        }
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }

    @GetMapping(value = "/busy-employees")
    ResponseEntity<Response<List<Employee>>> getBusyEmployees() {
        Response response = new Response<List<Employee>>();
        response.setStatus(HttpStatus.OK.value());
        response.setData(this.employeeService.getEmployees().stream()
                .filter(Employee::isBusy).collect(Collectors.toList()));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping(value = "/subscribe-all")
    ResponseEntity<Response<List<Employee>>> subscribeEmployees() {

        this.employeeRepository.deleteAll();

        List<Employee> employees = new ArrayList<>();

        employees.add(new Employee("Andres", Rol.DIRECTOR));
        employees.add(new Employee("Berta", Rol.DIRECTOR));
        employees.add(new Employee("Carolina", Rol.OPERATOR));
        employees.add(new Employee("Martha", Rol.SUPERVISOR));
        employees.add(new Employee("Susana", Rol.OPERATOR));
        employees.add(new Employee("Mario", Rol.OPERATOR));
        employees.add(new Employee("Luis", Rol.SUPERVISOR));
        employees.add(new Employee("Laura", Rol.OPERATOR));
        employees.add(new Employee("Camilo", Rol.OPERATOR));
        employees.add(new Employee("Jhonatan", Rol.SUPERVISOR));
        this.employeeService.subscribeEmployees(employees);

        this.employeeRepository.saveAll(employees);

        Response response = new Response();
        response.setStatus(HttpStatus.OK.value());
        response.setMessage("employees rolled successful!");
        response.setData(employees);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping(value = "/unsubscribe-all")
    ResponseEntity<Response> unsubscribeEmployees() {
        this.employeeService.unsubscribeEmployees();

        Response response = new Response();
        response.setStatus(HttpStatus.OK.value());
        response.setMessage("employees unenrolled successful!");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
