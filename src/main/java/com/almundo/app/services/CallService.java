package com.almundo.app.services;

import com.almundo.app.business.CallCenter;
import com.almundo.app.model.Call;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by sebastian on 5/9/19.
 */
@Service
public class CallService implements ICallService {
    @Autowired
    private CallCenter callCenter;

    @Override
    public List<Call> getAllCalls() {
        List<Call> calls = new ArrayList<>();
        calls.addAll(this.callCenter.getActiveCalls());
        calls.addAll(this.callCenter.getWaitingCalls());
        return calls;
    }

    @Override
    public Call getByIdCaller(String idCaller) {
        Optional<Call> opCall = this.callCenter.getActiveCalls().stream()
                .filter(call -> call.getCaller().getId().equals(idCaller)).findFirst();

        if (opCall.isPresent()) {
            return opCall.get();
        }

        opCall = this.callCenter.getWaitingCalls().stream()
                .filter(call -> call.getCaller().getId().equals(idCaller)).findFirst();

        if (opCall.isPresent()) {
            return opCall.get();
        }

        return null;
    }


    public CallCenter getCallCenter() {
        return callCenter;
    }

    public void setCallCenter(CallCenter callCenter) {
        this.callCenter = callCenter;
    }
}
