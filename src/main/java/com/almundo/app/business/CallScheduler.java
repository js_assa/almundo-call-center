package com.almundo.app.business;

import com.almundo.app.model.Call;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by sebastian on 5/8/19.
 */
@Component
public class CallScheduler {

    @Autowired
    private CallCenter callCenter;

    @Scheduled(fixedDelay = 2000)
    public void checkPendingsCalls() {
        Call call = this.callCenter.getWaitingCalls().poll();
        if (call != null) {
            this.callCenter.receiveNewCall(call.getCaller());
        }
    }

    public CallCenter getCallCenter() {
        return callCenter;
    }

    public void setCallCenter(CallCenter callCenter) {
        this.callCenter = callCenter;
    }
}
