package com.almundo.app.services;

import com.almundo.app.model.Call;

import java.util.List;

/**
 * Created by sebastian on 5/9/19.
 */
public interface ICallService {
    List<Call> getAllCalls();
    Call getByIdCaller(String idCaller);
}
