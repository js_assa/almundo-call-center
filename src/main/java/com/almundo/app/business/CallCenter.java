package com.almundo.app.business;

import com.almundo.app.model.Call;
import com.almundo.app.model.Caller;
import com.almundo.app.model.Employee;
import com.almundo.app.model.Response;
import com.almundo.app.util.*;
import com.almundo.app.util.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.time.LocalTime;
import java.util.*;

/**
 * Created by sebastian on 5/8/19.
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class CallCenter {
    private String currentUri;
    private List<Employee> employees = new ArrayList<>();
    private List<Call> activeCalls = new ArrayList<>();
    private Queue<Call> waitingCalls = new ArrayDeque<>();

    @Autowired
    private Properties properties;

    public Optional<Employee> getFreeEmployee() {
        return employees.stream().sorted(Comparator.comparing(Employee::getRol))
                .filter(employee -> !employee.isBusy()).findFirst();
    }

    public void subscribeEmployees(List<Employee> employees) {
        this.employees.addAll(employees);
    }

    public void unsubscribeEmployees() {
        this.employees.clear();
    }

    public Response<Call> receiveNewCall(Caller caller) {
        Response response = new Response();
        Call call;

        Optional<Employee> optEmployee = this.getFreeEmployee();
        if (optEmployee.isPresent()) {
            Employee employee = optEmployee.get();
            call = new Call(employee, caller, this, this.properties);
            call.startCall();
            this.activeCalls.add(call);
        } else {
            call = new Call(caller, this, this.properties);
            this.waitingCalls.add(call);
        }
        if (currentUri == null) {
            currentUri = ServletUriComponentsBuilder.fromCurrentRequest().toUriString();
        }
        String message = String.format("%s%s", currentUri, call.getCaller().getId());
        response.setStatus(HttpStatus.OK.value());
        response.setMessage(message);
        response.setData(call);
        return response;
    }

    public void finishCall(Call call) {
        call.setStatusCall(StatusCall.FINISHED);
        call.setTimeFinish(LocalTime.now());
        call.getEmployee().setBusy(false);
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Call> getActiveCalls() {
        return activeCalls;
    }

    public void setActiveCalls(List<Call> activeCalls) {
        this.activeCalls = activeCalls;
    }

    public Queue<Call> getWaitingCalls() {
        return waitingCalls;
    }

    public void setWaitingCalls(Queue<Call> waitingCalls) {
        this.waitingCalls = waitingCalls;
    }
}
