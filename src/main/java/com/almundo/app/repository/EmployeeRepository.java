package com.almundo.app.repository;

import com.almundo.app.model.Employee;
import com.almundo.app.util.Rol;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

/**
 * Created by sebastian on 8/11/19.
 */
public interface EmployeeRepository extends MongoRepository<Employee, String> {
    List<Employee> findByName(String name);
    List<Employee> findByRol(String rol);
    List<Employee> findByRol(Rol rol);
}
