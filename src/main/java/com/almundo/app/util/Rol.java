package com.almundo.app.util;

/**
 * Created by sebastian on 5/8/19.
 */
public enum Rol {
    OPERATOR(1),
    SUPERVISOR(2),
    DIRECTOR(3);

    private int level;

    Rol(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }
}
