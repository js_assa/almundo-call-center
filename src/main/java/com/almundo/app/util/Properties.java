package com.almundo.app.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * Created by sebastian on 5/9/19.
 */

@Configuration
@PropertySource("classpath:application.properties")
public class Properties {
    @Autowired
    private Environment env;

    public String getProperty(String property) {
        return env.getProperty(property);
    }
}
