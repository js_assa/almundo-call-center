package com.almundo.app.services;

import com.almundo.app.business.CallCenter;
import com.almundo.app.model.Call;
import com.almundo.app.model.Caller;
import com.almundo.app.model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by sebastian on 5/8/19.
 */
@Service
public class DispatcherService implements Dispatcher {

    @Autowired
    private CallCenter callCenter;

    @Override
    public Response<Call> dispatchCall(Caller caller) {
        return callCenter.receiveNewCall(caller);
    }

    public CallCenter getCallCenter() {
        return callCenter;
    }

    public void setCallCenter(CallCenter callCenter) {
        this.callCenter = callCenter;
    }
}
