package com.almundo.app.controllers;

import com.almundo.app.model.Call;
import com.almundo.app.model.Caller;
import com.almundo.app.model.Response;
import com.almundo.app.services.Dispatcher;
import com.almundo.app.services.ICallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

/**
 * Created by sebastian on 5/8/19.
 */
@RestController
@RequestMapping("calls")
public class CallController {

    @Autowired
    private Dispatcher dispatcherService;

    @Autowired
    private ICallService callService;

    @PostMapping(value = "/")
    ResponseEntity<Response> receiveCall() {
        Caller caller = new Caller();
        caller.setId(UUID.randomUUID().toString());
        return new ResponseEntity<>(dispatcherService.dispatchCall(caller), HttpStatus.OK);
    }

    @GetMapping(value = "/")
    List<Call> getAllCalls() {
        return callService.getAllCalls();
    }

    @GetMapping(value = "/{idCaller}")
    ResponseEntity<Call> getByIdCaller(@PathVariable String idCaller) {
        return new ResponseEntity<>(this.callService.getByIdCaller(idCaller), HttpStatus.OK);
    }
}
