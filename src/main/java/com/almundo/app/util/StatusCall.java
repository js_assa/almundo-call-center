package com.almundo.app.util;

/**
 * Created by sebastian on 5/9/19.
 */
public enum StatusCall {
    INITIAL("inicial"),
    ACTIVE("activa"),
    WAITING("en espera"),
    FINISHED("finalizada"),
    CANCELED("cancelada");

    private String status;

    StatusCall(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
