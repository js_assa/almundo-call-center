# Almundo test

## Descripción de la aplicación

Esta aplicación está construída con java 8, usando el framework spring-boot, y simula el funcionamiento de un call center de la siguiente manera:

1. Para simular recibir una llamada, se debe enviar una petición tipo POST a http://ip:port/calls. 
El software buscará empleados disponibles para atender la llamada, en caso de no haber ningún empleado disponible, la llamada se encolará y se retornará
un json con la siguiente estructura:

~~~~
{
    "status": 200,
    "message": "http://localhost:8089/calls/db4f7e36-c342-4790-b7d3-88fcc06ebd11",
    "data": {
        "employee": null,
        "caller": {
            "id": "db4f7e36-c342-4790-b7d3-88fcc06ebd11"
        },
        "timeStart": null,
        "timeFinish": null,
        "statusCall": "WAITING"
    }
}
~~~~

Si por el contrario, se encuentra un empleado disponible, se creará un nuevo objeto con la información de la llamada, el empleado 
cambiará a estado OCUPADO y la llamada a estado ACTIVO. Y tendrá la siguiente estructura:
~~~~
{
    "status": 200,
    "message": "http://localhost:8089/calls/3c902468-3fd6-4372-954b-1ae01103344c",
    "data": {
        "employee": {
            "name": "Susana",
            "rol": "OPERATOR"
        },
        "caller": {
            "id": "3c902468-3fd6-4372-954b-1ae01103344c"
        },
        "timeStart": "17:23:55.159",
        "timeFinish": null,
        "statusCall": "ACTIVE"
    }
}
~~~~

2. Cuenta con un método para subscribir empleados por defecto, para ejecutar este método, se debe hacer una petición POST a: 
http://ip:port/employees/subscribe-all

## Ejecutar la aplicación

1. Se debe ejecutar el siguiente comando para la compilación y publicación de la aplicación: mvn clean package install -DskipTests

2. Una vez publicada, se puede correr el servicio de la siguiente manera: java -jar target\test-1.0.0.jar --server.port=8089