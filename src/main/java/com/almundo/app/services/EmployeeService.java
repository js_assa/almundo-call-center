package com.almundo.app.services;

import com.almundo.app.business.CallCenter;
import com.almundo.app.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by sebastian on 5/9/19.
 */
@Service
public class EmployeeService {

    @Autowired
    private CallCenter callCenter;
    public void subscribeEmployees(List<Employee> employees) {
        callCenter.subscribeEmployees(employees);
    }

    public void unsubscribeEmployees() {
        callCenter.unsubscribeEmployees();
    }

    public List<Employee> getEmployees() {
        return callCenter.getEmployees();
    }

    public CallCenter getCallCenter() {
        return callCenter;
    }

    public void setCallCenter(CallCenter callCenter) {
        this.callCenter = callCenter;
    }
}
