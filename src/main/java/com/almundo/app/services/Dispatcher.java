package com.almundo.app.services;

import com.almundo.app.model.Call;
import com.almundo.app.model.Caller;
import com.almundo.app.model.Response;

/**
 * Created by sebastian on 5/8/19.
 */
public interface Dispatcher {
    Response<Call> dispatchCall(Caller caller);
}
