package com.almundo.app.model;

import com.almundo.app.business.CallCenter;
import com.almundo.app.util.Properties;
import com.almundo.app.util.StatusCall;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by sebastian on 5/8/19.
 */
@JsonIgnoreProperties(value = { "callCenter", "duration", "properties" })
public class Call implements Model {
    private Employee employee;
    private Caller caller;
    private LocalTime timeStart;
    private LocalTime timeFinish;
    private int duration;
    private StatusCall statusCall = StatusCall.INITIAL;

    private CallCenter callCenter;
    private Properties properties;

    public Call() { }

    public Call(Caller caller, CallCenter callCenter, Properties properties) {
        this.caller = caller;
        this.callCenter = callCenter;
        this.statusCall = StatusCall.WAITING;
        this.properties = properties;
    }

    public Call(Employee employee, Caller caller, CallCenter callCenter, Properties properties) {
        this.employee = employee;
        this.caller = caller;
        this.callCenter = callCenter;
        this.properties = properties;
    }

    public void startCall() {
        this.statusCall = StatusCall.ACTIVE;
        this.timeStart = LocalTime.now();
        this.employee.setBusy(true);

        int timeCallMin = Integer.parseInt(this.properties.getProperty("timecall.min"));
        int timeCallMax = Integer.parseInt(this.properties.getProperty("timecall.max"));
        duration = new Random().nextInt((timeCallMax - timeCallMin) + 1) + timeCallMin;
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            try {
                TimeUnit.SECONDS.sleep(duration);
                this.callCenter.finishCall(this);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    public void setCallCenter(CallCenter callCenter) {
        this.callCenter = callCenter;
    }

    public CallCenter getCallCenter() {
        return callCenter;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Caller getCaller() {
        return caller;
    }

    public void setCaller(Caller caller) {
        this.caller = caller;
    }

    public LocalTime getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(LocalTime timeStart) {
        this.timeStart = timeStart;
    }

    public LocalTime getTimeFinish() {
        return timeFinish;
    }

    public void setTimeFinish(LocalTime timeFinish) {
        this.timeFinish = timeFinish;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public StatusCall getStatusCall() {
        return statusCall;
    }

    public void setStatusCall(StatusCall statusCall) {
        this.statusCall = statusCall;
    }
}
